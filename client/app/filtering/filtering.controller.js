'use strict';

angular.module('solinorHackathonApp')
  .controller('FilteringCtrl', function ($scope, $http) {

    $scope.select2OptionsServers = {
        'multiple': true,
        'simple_tags': true,
        'tags': ['tag1', 'tag2', 'tag3', 'tag4']  // Can be empty list.
    };
    $scope.select2OptionsTechnologies = {
        'multiple': true,
        'simple_tags': true,
        'tags': ['tag1', 'tag2', 'tag3', 'tag4']  // Can be empty list.
    };

  	$scope.db = [];
  	$scope.locations = [];
  	$scope.example1model = [];
    $http.get('https://crappy.herokuapp.com/sites/').success(function(data) {
      $scope.db = data;
    });

	$http.get('https://crappy.herokuapp.com/locations/').success(function(data) {
		      	$scope.locations = data;
		      	var temp = [];
				
				$scope.locations.forEach(function(elem) {
					temp.push(elem.city_accent);
				});
				console.log(temp);
		      	$scope.countries = temp;

				$(".js-example-basic-multiple").select2();
		      	return temp;
		    });

  });
