'use strict';

angular.module('solinorHackathonApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('filtering', {
        url: '/filtering',
        templateUrl: 'app/filtering/filtering.html',
        controller: 'FilteringCtrl'
      });
  });