'use strict';

describe('Controller: FilteringCtrl', function () {

  // load the controller's module
  beforeEach(module('solinorHackathonApp'));

  var FilteringCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FilteringCtrl = $controller('FilteringCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
