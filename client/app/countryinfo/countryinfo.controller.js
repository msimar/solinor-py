'use strict';

angular.module('solinorHackathonApp')
  .controller('CountryinfoCtrl', function ($scope, $http, $stateParams) {
    console.log($stateParams.ccode);

  	$scope.db = [];
    $http.get('https://crappy.herokuapp.com/sites_by_country/' + $stateParams.ccode.toLowerCase()).success(function(data) {
      $scope.db = data;
      console.log(data);
    });

    $scope.topTech = [];
	$http.get('https://crappy.herokuapp.com/top_technologies_by_country/' + $stateParams.ccode.toLowerCase()).success(function(data) {  	
    	$scope.topTech = data;
    	console.log(data);


		var x = d3.scale.linear()
		    .domain([0, 400])
		    .range([100, 400]);

		d3.select(".chart")
		  .selectAll("div")
		    .data(data)
		  .enter().append("div")
		    .style("width", function(d) { return x(d.count * 5) + "px"; })
		    .text(function(d) { return d.value + "(" + d.count + ")"; });	
	});


  });
