'use strict';

angular.module('solinorHackathonApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('countryinfo', {
        url: '/countryinfo',
        templateUrl: 'app/countryinfo/countryinfo.html',
        controller: 'CountryinfoCtrl'
      })
      .state('countryccode', {
        url: '/countryinfo/:ccode',
        templateUrl: 'app/countryinfo/countryinfo.html',
        controller: 'CountryinfoCtrl'
      });
  });