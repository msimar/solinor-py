'use strict';

describe('Controller: CountryinfoCtrl', function () {

  // load the controller's module
  beforeEach(module('solinorHackathonApp'));

  var CountryinfoCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CountryinfoCtrl = $controller('CountryinfoCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
