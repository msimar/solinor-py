'use strict';

angular.module('solinorHackathonApp')
  .controller('MainCtrl', function ($scope, $http, $location) {
  	$scope.db = [];

  	$scope.toplist = [];

    $http.get('https://crappy.herokuapp.com/sites/').success(function(data) {
      $scope.db = data;
    });

    $http.get('https://crappy.herokuapp.com/top_technologies/').success(function(data) {
        var log = '{ "name": "flare", "children": [' ;
        var size = data.length - 1;
        var i = 1;
        var maxValue = 0 ;
        data.forEach(function(value) {
          i++;
          log = log + "{"

          log = log + '"name": "an-' + i + '",';
          log = log + '"children": [{' ;

          log = log + '"name": "'  + value.value + "\",";
          log = log + '"size": ' + value.count ;

          if( value.count > maxValue ){
            maxValue = value.count ;
          }

          log = log + "}]}";
          if( size > 0 ) {
            log = log + ",";
            size--;
          }
        }, log);
        log  = log + ']}';
   
        var root = JSON.parse(log);

      //d3.json("app/flare.json", function(error, root) {
        var node = svg.selectAll(".node")
            .data(bubble.nodes(classes(root))
            .filter(function(d) { return !d.children; }))
            .enter().append("g")
            .attr("class", "node")
            .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

        node.append("title")
            .text(function(d) { return d.className + ": " + format(d.value); });

        node.append("circle")
            .attr("r", function(d) { return d.r; })
            .style("fill", function(d) { return color(d.packageName); });

        node.append("text")
            .attr("dy", ".3em")
            .style("text-anchor", "middle")
            .style("font-size", function(d) { return ( (d.value * 2 ) / maxValue) + "em"; })
            .text(function(d) { return d.className.substring(0, d.r / 3); });
      //});

      // Returns a flattened hierarchy containing all leaf nodes under the root.
      function classes(root) {
        var classes = [];

        function recurse(name, node) {
          if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
          else classes.push({packageName: name, className: node.name, value: node.size});
        }

        recurse(null, root);
        return {children: classes};
      }

      d3.select(self.frameElement).style("height", diameter + "px");

    });
    
    $scope.getFiveAndBlank = function() {
      var list = [];
      $scope.db.forEach(function(elem) {
        if (elem.company_name.trim().length > 0 && elem.logo_url.trim().length > 0) {
          list.push(elem);
        }
      });
    	return list.splice(0,5);
    }

    $scope.go = function( path ){
      $location.path(path);
    }

    var diameter = 480,
        format = d3.format(",d"),
        color = d3.scale.category20c();

    var bubble = d3.layout.pack()
        .sort(null)
        .size([diameter, diameter])
        .padding(1.5);

    var svg = d3.select("#bubble").append("svg")
        .attr("width", diameter)
        .attr("height", diameter)
        .attr("class", "bubble");



      


  });
