'use strict';

angular.module('solinorHackathonApp')
  .controller('StatsCtrl', function ($scope, $http) {

  	

	$http.get('https://crappy.herokuapp.com/top_technologies_by_countries/').success(function(data) {

		data.sort(function(a, b) {
		  if (a[2] > b[2]) {
		    return -1;
		  }
		  if (a[2] < b[2]) {
		    return 1;
		  }
		  return 0;
  		});

		var gdpData = {};

		data.forEach(function(elem) {
			gdpData[elem[0].toUpperCase()] = 0;
		});

		Object.keys(gdpData).forEach(function(elem) {

			var sum = 0;
			data.forEach(function(elem) {
				sum += elem[2];
			});
			gdpData[elem] = sum;

		});

	  $('#world-map').vectorMap({
	    map: 'world_mill_en',
	    series: {
	      regions: [{
	        values: gdpData,
	        scale: ['#C8EEFF', '#0071A4'],
	        normalizeFunction: 'polynomial'
	      }]
	    },
	    onRegionTipShow: function(e, el, code){
			$http.get('https://crappy.herokuapp.com/top_technologies_by_country/us').success(function(data) {  	

			});
			var techs = "";

			var i = 0;
			data.forEach(function(elem) {
				if (elem[0] == code.toLowerCase() && i < 5) {
					techs += "<li>" + elem[1] + ": " + elem[2] + "</li>";
					i++;
				}
			});

	      el.html(el.html()+techs);
	    },
	    onRegionClick: function (event, code) {
    		window.location.href = "/countryinfo/" + code
		},
	  });


		/*$('#world-map').vectorMap({map: 'world_mill_en',
			onRegionTipShow: function(e, el, code){
      		el.html(el.html()+' (GDP - '+gdpData[code]+')');
    	}});*/
	});
  });
