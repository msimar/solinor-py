'use strict';

angular.module('solinorHackathonApp')
  .controller('NavbarCtrl', function ($scope, $location) {
    $scope.menu = [{
      'title': 'Home',
      'link': '/'
    },{
      'title': 'Explore',
      'link': '/filtering'
    },{
      'title': 'Map',
      'link': '/stats'
    }
    ];

    $scope.isCollapsed = true;

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });